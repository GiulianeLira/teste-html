import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TesteHtml {

    private WebDriver driver;

    @Before
    public void abrir () {
        System.setProperty("webdriver.gecko.driver","C:/Driver/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get ("https://testpages.herokuapp.com/styled/basic-html-form-test.html");

    }

    @After
    public void sair () {driver.quit();}

    @Test
    public void testUsername() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[1]/td/input")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[1]/td/input")).sendKeys("qualquer");
        Thread.sleep(3000);
        Assert.assertEquals("testUsername", driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[1]/td/input")).getText());


    }
    @Test
    public void testPassword() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.xpath( "//*[@id=\"HTMLFormElements\"]/table/tbody/tr[2]/td/input")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[2]/td/input")).sendKeys("qualquer");
        Thread.sleep(3000);
        Assert.assertEquals("testPassword", driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[2]/td/input")).getText());

    }
    @Test
    public void testComment() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[3]/td/textarea")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("\"//*[@id=\\\"HTMLFormElements\\\"]/table/tbody/tr[3]/td/textarea\"")).sendKeys("qualquer");
        Thread.sleep(3000);
        Assert.assertEquals("testComment", driver.findElement(By.xpath("//*[@id=\"HTMLFormElements\"]/table/tbody/tr[3]/td/textarea")).getText());

    }

}
